@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h6>Users Table</h6>
        </div>
        <div class="card-body">
            <form class="mb-4" action="{{ route('user.index') }}" method="GET">
                <div class="input-group">
                    <input class="form-control col-4 mr-2" type="text" name="search" placeholder="Search by name or email" value="{{ request('search') }}">
                    <button class="btn btn-primary" type="submit">Search</button>
                </div>
            </form>
            <table class="table">
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Position</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($users as $d)
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->email }}</td>
                        <td>{{ $d->position->name }}</td>
                        <td>
                            <a class="btn btn-success btn-sm" href="{{ route('user.show', [ $d->id ]) }}">Show</a>
                            <a class="btn btn-warning btn-sm" href="{{ route('user.edit', [ $d->id ]) }}">Edit</a>
                            <form action="{{ route('user.destroy', [ $d->id ]) }}" method="post" style="display:inline;">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('Are you sure ?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary"> + Add Data</a>

            {{ $users->links('pagination::bootstrap-5') }}
        </div>
    </div>
</div>
@endsection
