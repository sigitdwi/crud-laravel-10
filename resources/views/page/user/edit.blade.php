@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h6>Edit Users</h6>
        </div>
        <div class="card-body">
            <form action="{{ route('user.update', [$user->id]) }}" method="post">
                @csrf
                @method('PUT')

                <div class="mb-3">
                    <label for=""> Name</label>
                    <input value="{{ $user->name}}" type="text" name="name" class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <div class="text-danger"> {{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for=""> Email</label>
                    <input value="{{ $user->email}}" type="text" name="email" class="form-control @error('email') is-invalid @enderror">
                    @error('email')
                    <div class="text-danger"> {{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary btn-sm">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection
