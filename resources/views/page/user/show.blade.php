@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h6>Show Users</h6>
        </div>
        <div class="card-body">
            {{ $user }} <br/>
            <h5 class="card-title">{{ $user->name }}</h5>
            <p class="card-text"><strong>Email:</strong> {{ $user->email }}</p>
            <a href="{{ route('user.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
</div>
@endsection
