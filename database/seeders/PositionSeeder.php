<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Position::insert([
            [
                'name' => 'Manager',
                'remark' => '-',
            ],
            [
                'name' => 'Supervisor',
                'remark' => '-',
            ],
            [
                'name' => 'Staff',
                'remark' => '-',
            ]
        ]);
    }
}
