<?php
namespace App\Services;

class ExcelExportService
{
    public function exportData()
    {
        $html = '<table border="1">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
            </tr>
            <tr>
                <td>1</td>
                <td>John Doe</td>
                <td>john@example.com</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Jane Doe</td>
                <td>jane@example.com</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Bob Smith</td>
                <td>bob@example.com</td>
            </tr>
        </table>';
        
        return response()->streamDownload(
            fn () => print($html),
            'report.xls',
            ['Content-Type' => 'application/xls']
        );
    }
}
