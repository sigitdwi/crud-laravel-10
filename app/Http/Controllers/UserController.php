<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;

        $users = User::search($search)->paginate(2);
        return view('page.user.index', compact('users'));
    }

    public function create()
    {
        $position = Position::all();
        return view('page.user.create', compact('position'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users, email',
        ]);
       $user = new User([
        'name' => $request->name,
        'email' => $request->email,
        'password' => bcrypt('12345678'),
       ]);
       $user->save();

       return redirect()->route('user.index');
    }

    public function edit(User $user)
    {
        return view('page.user.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
        ]);
        
        $user->fill($request->all())->update();
        return redirect()->route('user.index');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('user.index');
    }

    public function show(User $user)
    {
        return view('page.user.show', compact('user'));
    }

    public function test(User $user)
    {
        return 'test';
    }

    

}
