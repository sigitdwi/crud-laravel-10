<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $cridentials = $request->only('email', 'password');
        if(Auth::attempt($cridentials)){
           $token = Auth::user()->createToken('api_token')->plainTextToken;
           return response()->json(['token' => $token], 200);
        }

        return response()->json(['message' => 'invalid cridential'], 401);
    }

    public function profile(Request $request)
    {
        return response()->json(new UserResource($request->user()), 200);
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json(['message' => 'logout successfull'], 200);
    }
}
