<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(Request $req)
    {
        if(Auth::check()){
            return redirect('/');
        }
        return view('page.login.index');
    }

    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return redirect()->intended('/')->withSuccess('Login Sukses');
        }
        return redirect('login')->withErrors([
            'message' => 'invalid cridetial'
        ])->withInput();
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
