<?php

//use Illuminate\Http\Request;
use App\Services\ExcelExportService;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PositionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/




Route::get('/login', [LoginController::class, 'index'])->name('login.index');
Route::post('/login', [LoginController::class, 'login'])->name('login');

Route::middleware(['auth'])->group(function () {
    Route::get('/', [HomeController::class, 'index']);
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

    // Route::get('/user', [UserController::class, 'index'])->name('user.index');
    // Route::get('/user/create', [UserController::class, 'create'])->name('user.create');
    // Route::post('/user', [UserController::class, 'store'])->name('user.store');

    // Route::get('/user/{user}/edit', [UserController::class, 'edit'])->name('user.edit');
    // Route::put('/user/{user}', [UserController::class, 'update'])->name('user.update');
    
    // Route::delete('/user/{user}', [UserController::class, 'destroy'])->name('user.destroy');
    // Route::get('/user/{user}', [UserController::class, 'show'])->name('user.show');

    Route::get('/user/test', [UserController::class, 'test'])->name('user.test');
    Route::resource('user', UserController::class);
    Route::resource('position', PositionController::class);
    Route::get('/export', function(){
        $export = new ExcelExportService();
        return $export->exportData();
    });
});

